#! /usr/bin/env node
/**
 *  Name:  cphub-catalog.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const {program } = require('commander');

program
    .version('0.5.0')
    .command('list', 'lists catalog items')
    .command('info <sku>', 'retrieves detailed catalog item information.')
    .parse( process.argv);



    