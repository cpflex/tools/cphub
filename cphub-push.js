#! /usr/bin/env node
/**
 *  Name:  cphub-push.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require('request');
var tools = require('./tools');
var path = require("path");
const {program } = require('commander');

require('console.table');
var fs = require('fs');
program
    .description("Pushes a tagged image to a package", {
        "imagetype": 'Required image media type',
        "imagefile": 'Required imagefile to push to registry.',
        "pathuri": 'Required pathuri URI having "[/]node1/node2/...:tag" or "//hub.cpflex.tech/node1/node2/...:tag" formats.',
        "tagspec": 'tag specifications in JSON .'
    })
    .option('-s, --spec <specfile>', 'json file containing the image specification merged with command line.')
    .option('-l, --latest', 'Update latest tag with reference to this tagged image.')    
    .arguments('<imagetype> <imagefile> <pathuri> [tagspec]')    
    .action( function( imagetype, imagefile, pathuri, tagspec){
        _imagetype = imagetype;
        _imagefile = imagefile;
        _pathuri = pathuri;
        _tagspec = tagspec;
    });

tools.addCommonOptions(program);
program.parse( process.argv);
tools.logHeader(program);

//Load input spec if defined.
var inputspec = {
};
const bHaveSpec = !(program.spec ===undefined);

if( bHaveSpec)
{
    var data = fs.readFileSync(program.spec, 'utf8');
    tools.mergeObjects( inputspec, JSON.parse( data.toString()));
}

//Merge commandline if defined
if( !(_tagspec === undefined))
{
    tools.mergeObjects( inputspec, JSON.parse(_tagspec));
}

//Create the default structure.
var input = {
    etid: "cphub-push",
    taguri: _pathuri,
    images: [
        {  
            imagetype: _imagetype,
        }
    ],
    attributes : {
        "filename": path.basename(_imagefile)
    },
    references : [],
    constraints: [],
    attributes: {},
    state: 0
};

//Merge with specfile if defined.
if( bHaveSpec && inputspec != null)
{
    tools.mergeObjects( input, inputspec);
}

var data = fs.readFileSync( _imagefile,{encoding: "base64"});

//Dump information about the push.
if( program.verbose === true) {
    console.log('');
    console.log('Pushing Image: ');
    console.log(' file:       '  + _imagefile);
    console.log(' imagetype:  ' + _imagetype);
    console.log(' size:       ' +  data.length/1024 + ' kBytes');
    console.log(' taguri:     ' + _pathuri );
    console.log(' tag details ' );
    console.log( JSON.stringify( input, null, 2));
    console.log(' ');
}

//Attach file to input.
input.images[0].image = data;

function UpdateTag( input, api, handler){

    var req = tools.getApiRequest( program, api, input);
    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);

        handler( resp.resultcode, req.body.length);
    });
}

UpdateTag( input, "/images/push", function( rc, ctSent){

    if( program.verbose ===true)
        console.log("Pushed: " + ctSent/1024 + " kBytes.");

    if( program.latest === true && rc == 1)
    {
        var pos = _pathuri.indexOf(':');
        var latesturi = _pathuri.substr( 0, pos) + ":latest";
        var latest = {
            etid: "cphub-push",
            taguri: latesturi,
            state: 0,
            references : [
                _pathuri
            ],
            created     : new Date(),
            description : input.description,
            constraints: input.constraints,
            attributes:input.attributes
        };

        //MBM 2021/07/22 To Support Tag Types, we set the tag type
        //attribute to reference.
        latest.attributes.tagtype = "reference";

        UpdateTag( latest, "/tags/update", function(rc, ctSent) {
            if( program.verbose ===true)
                console.log("Updated: " + latesturi );
        });
    }
});