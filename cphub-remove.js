#! /usr/bin/env node
/**
 *  Name:  cphub-remove.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require('request');
var tools = require('./tools');
const util = require('util');
const {program } = require('commander');

require('console.table');
var fs = require('fs');
program
    .description("Removes a tag or package from the hub.  This permanently deletes the item and is not recoverable.  Cannot remove container nodes unless child nodes are first removed.", {
        "pathuri": 'Required pathuri URI having "[/]node1/node2/...:tag" or "//hub.cpflex.tech/node1/node2/...:tag" formats.'
    })
    .arguments('<pathuri>')
    .action( function( pathuri){
        _pathuri = pathuri;
    });
tools.addCommonOptions(program);    
program.parse( process.argv);
tools.logHeader(program);


var input = {
    etid : "cphub-remove"
};

var pos = _pathuri.indexOf(':');
var apipath;
if( pos >= 0) {
    input.taguri = _pathuri;
    apipath = "/tags/remove";
} else {
    input.nodeuri = _pathuri;
    apipath = "/nodes/remove";
}

//Dump information about the push.
if( program.verbose === true) {
    console.log(' command details ' );
    console.log( JSON.stringify( input, null, 2));
    console.log(' ');
}

var req = tools.getApiRequest( program, apipath, input);

request( req, (error, response, body) => {
    var resp = tools.parseResponseBody(error, response, body);
    console.log("item removed");
});

