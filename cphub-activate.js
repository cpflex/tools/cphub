#! /usr/bin/env node
/**
 *  Name:  cphub-activate.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require('request');
var tools = require('./tools');
const util = require('util');
const {program } = require('commander');

require('console.table');
var fs = require('fs');

/**
 * Function retrieves Tag information.
 * @param {*} taguri 
 */
function GetTagInfo( taguri, handler) {

    var input = {
        "etid":"cphub-activate",
        "taguri": taguri
    };

    var req = tools.getApiRequest( program, "/tags/get", input);
    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        handler( resp.tag);
    });
}

/**********************************************************************
 * Main Script
 *********************************************************************/

program
    .description("Marks a tag as active.  Tag will  be visibile in list and constraint queries by default.", {
        "pathuri": 'Required tag pathuri URI having "[/]node1/node2/...:tag" or "//hub.cpflex.tech/node1/node2/...:tag" formats.',
    })
    .arguments('<pathuri>')
    .action( function(  pathuri){
        _pathuri = pathuri;
    });
tools.addCommonOptions(program);
program.parse( process.argv);
tools.logHeader(program);

//Get the tag and the update the state,
GetTagInfo( _pathuri, (info)=>{
    var input = {
        etid: "cphub-tag/update",
        taguri: _pathuri,
        references : [],
        constraints : [],
        attributes : {},
        state:0
    };
    
    tools.mergeObjects( input, info);    
    input.state = 0;

    //Dump information about the push.
    if( program.verbose === true) {
        console.log(' Updated Tag Information' );
        console.log( JSON.stringify( input, null, 2));
        console.log(' ');
    }
    var req = tools.getApiRequest( program, "/tags/update", input);
    var bytesSent = req.body.length;

    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        console.log("Tag activated");
    });
});

