set CPHUB_URL="http://192.168.192.158:8030"
set CODEPOINT=c175b6d8-540c-4031-8467-9ef05550c6aa
set CODEPOINT_ENG=f19b8040-e58d-43fc-b1c3-b5928357f152
set CODEPOINT_DEMO=3450a0e3-71f5-475b-acd0-dc81f092f205
set MATHEWS=3450a0e3-71f5-475b-acd0-dc81f092f21
set INVALID=3450a0e3-71f5-475b-acd0-dc81f092ab35

set CPFLEX_APIKEY="acctid|%CODEPOINT%"

cphub create /codepoint container -a public

cphub create /codepoint/pkg_pub  container -a public
cphub create /codepoint/pkg_pub/a  package -a public
cphub create /codepoint/pkg_pub/b  package -a public

cphub create /codepoint/pkg_act  container -a account
cphub create /codepoint/pkg_act/a  package -a account
cphub create /codepoint/pkg_act/b  package -a account

cphub create /codepoint/pkg_prv  container -a private
cphub create /codepoint/pkg_prv/a  package -a private
cphub create /codepoint/pkg_prv/b  package -a private


set CPFLEX_APIKEY="acctid|%CODEPOINT_ENG%"

cphub create /codepoint_eng container -a public

cphub create /codepoint_eng/pkg_pub  container -a public
cphub create /codepoint_eng/pkg_pub/a  package -a public
cphub create /codepoint_eng/pkg_pub/b  package -a public

cphub create /codepoint_eng/pkg_act  container -a account
cphub create /codepoint_eng/pkg_act/a  package -a account
cphub create /codepoint_eng/pkg_act/b  package -a account

cphub create /codepoint_eng/pkg_prv  container -a private
cphub create /codepoint_eng/pkg_prv/a  package -a private
cphub create /codepoint_eng/pkg_prv/b  package -a private

set CPFLEX_APIKEY="acctid|%CODEPOINT_DEMO%"

cphub create /codepoint_demo container -a public

cphub create /codepoint_demo/pkg_pub  container -a public
cphub create /codepoint_demo/pkg_pub/a  package -a public
cphub create /codepoint_demo/pkg_pub/b  package -a public

cphub create /codepoint_demo/pkg_act  container -a account
cphub create /codepoint_demo/pkg_act/a  package -a account
cphub create /codepoint_demo/pkg_act/b  package -a account

cphub create /codepoint_demo/pkg_prv  container -a private
cphub create /codepoint_demo/pkg_prv/a  package -a private
cphub create /codepoint_demo/pkg_prv/b  package -a private


set CPFLEX_APIKEY="acctid|%MATHEWS%"

cphub create /mathews container -a public

cphub create /mathews/pkg_pub  container -a public
cphub create /mathews/pkg_pub/a  package -a public
cphub create /mathews/pkg_pub/b  package -a public

cphub create /mathews/pkg_act  container -a account
cphub create /mathews/pkg_act/a  package -a account
cphub create /mathews/pkg_act/b  package -a account

cphub create /mathews/pkg_prv  container -a private
cphub create /mathews/pkg_prv/a  package -a private
cphub create /mathews/pkg_prv/b  package -a private
