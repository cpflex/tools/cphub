#! /usr/bin/env node
/**
 *  Name:  cphub-constraint-add.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const { program } = require("commander");
var tools = require("./tools");
const util = require("util");
require("console.table");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description("Adds a constraint on a node or tag..", {
      pathuri:
        'Required pathuri URI having "[/]node1/node2/..." or "//hub.cpflex.tech/node1/node2/..." formats.',
      constraint: "The constraint to add. [constraint name]:[path URI]",
    })
    .option("-v, --verbose", "Verbose output.")
    .arguments("<pathuri> <constraint>")
    .action((pathuri, constraint) => {
      var pos = pathuri.indexOf(":");
      let bIsTagRequest = pos >= 0;

      if (bIsTagRequest) {
        GetTagInfo(pathuri, (tag) => {
          const input = {
            etid: "cphub-constraint-add",
            taguri: pathuri,
            constraints: tag.constraints ? tag.constraints : [],
            references: tag.references,
            attributes: tag.attributes,
            state: tag.state,
          };
          input.constraints.push(constraint);

          var req = tools.getApiRequest(program, "/tags/update", input);
          request(req, (error, response, body) => {
            tools.parseResponseBody(error, response, body);
          });
        });
      } else {
        console.error("node constraint updates not currently supported.");
      }
    });

  tools.addCommonOptions(program);
  program.parse(process.argv);
  tools.logHeader(program);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}

/**
 * Function retrieves Node information.
 * @param {*} nodeuri
 */
function GetNodeInfo(nodeuri, handler) {
  var input = {
    etid: "cphub-ls",
    nodeuri: nodeuri,
  };

  var req = tools.getApiRequest(program, "/nodes/get", input);

  request(req, (error, response, body) => {
    var resp = tools.parseResponseBody(error, response, body);
    handler(resp.node);
  });
}

/**
 * Function retrieves Tag information.
 * @param {*} taguri
 */
function GetTagInfo(taguri, handler) {
  var input = {
    etid: "cphub-ls",
    taguri: taguri,
  };

  var req = tools.getApiRequest(program, "/tags/get", input);
  request(req, (error, response, body) => {
    var resp = tools.parseResponseBody(error, response, body);
    handler(resp.tag);
  });
}
