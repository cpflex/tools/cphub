#! /usr/bin/env node
/**
 *  Name:  cphub.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const { program } = require("commander");

program
  .version("0.5.0")
  .command(
    "activate <path:tag> ",
    "reactivates a deprecated tag making it visible."
  )
  .command("create  <path> [nodespec] ", "create a container  or package node.")
  .command("deprecate <path:tag> ", "deprecates a tag making it invisibile.")
  .command("info    <path> ", "get information about a node or tag")
  .command("list    <pathuri>", "lists the contents of a container or package.")
  .command(
    "push    <imagetype> <imagefile> <path:tag> [pushspec]",
    "push tagged image data to a package."
  )
  .command("remove  <path[:tag]> ", "removes node or tag from registry.")
  .command("update  <path:tag> [tagspec]", "update tag specifcation data.")
  .command("pull <pathuri>", "pull tagged image data from a package")
  .command(
    "copytags  <pathuri> <srcurl>",
    "Copies tags from one hub to another."
  )
  .command("catalog list", "lists catalog items")
  .command("catalog info <sku>", "retrieves detailed catalog item information.")
  .command(
    "constraint add <taguri> <constraint>",
    "adds a constraint to a tag."
  )
  .parse(process.argv);
