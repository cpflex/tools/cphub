#! /usr/bin/env node
/**
 *  Name:  cphub-pull.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require('request');
const {program } = require('commander');
var tools = require('./tools');

var fs = require('fs');
require('console.table');


program
    .description("Pulls a tagged image given path", {
        "pathuri": 'Required pathuri URI having "[/]node1/node2/...:tag[/imagetype]" or "//hub.cpflex.tech/node1/node2/...:tag[/imagetype]" formats.'
    })
    .option('-f, --file <filename>', 'Filename to save image data.' )
    .option('-o, --orig', 'Uses the original filename if stored as an attribute in the tag.' )
    .arguments('<pathuri>')
    .action( function( pathuri){
        _pathuri = pathuri;
    });

tools.addCommonOptions(program);

program.parse( process.argv);
tools.logHeader(program);


/**
 * Function retrieves Node information.
 * @param {*} nodeuri 
 */
function GetNodeInfo( nodeuri, handler) {

    var input = {
        "etid":"cphub-ls",
        "nodeuri": nodeuri
    };

    var req = tools.getApiRequest( program, "/nodes/get", input);

    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        handler( resp.node);
    });
}


/**
 * Function retrieves Tag information.
 * @param {*} taguri 
 */
function GetTagInfo( taguri, handler) {

    var input = {
        "etid":"cphub-ls",
        "taguri": taguri
    };

    var req = tools.getApiRequest( program, "/tags/get", input);
    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);        
        handler( resp.tag);
    });
}


/**
 * Function retrieves Tag information.
 * @param {*} taguri 
 */
function GetImage( taguri, handler) {

    var input = {
        "etid":"cphub-ls",
        "taguri": taguri
    };

    var req = tools.getApiRequest( program, "/images/pull", input);
    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        handler( resp.image);
    });
}

function PullImage( taguri, attributes){
    var filename;
    if( !(program.file === undefined)) {
        filename = program.file
    } else if(program.orig == true && !(attributes.filename === undefined)) {
        filename = attributes.filename;
    } else {
        filename = taguri.substr(2);
        filename = filename.replace( new RegExp("/","g"), "-");
        filename = filename.replace( new RegExp(":","g"), "--");
        filename +=".bin"
    }

    GetImage( taguri, function(image) {
        var buf = new Buffer.from(image.image, 'base64');
        fs.writeFileSync(filename, buf);

        console.log("Saving: " + filename);
    });
}

/******************************************************
 * Begin processing information.
******************************************************/

console.log('');
console.log('Pulling: ' + _pathuri);
console.log('');

GetNodeInfo( _pathuri, function(node){
    var pos = _pathuri.indexOf(':')
    var taguri = node.name + ":" + _pathuri.substr( pos+1);

    GetTagInfo( taguri, function(tag){

        if( program.verbose === true){
            console.log( "Tag information:")
            console.log( JSON.stringify(tag,null, 2));
        }

        if (tag == undefined) {
            console.error("Tag not found: " + taguri);
        } else {
            PullImage(taguri, tag.attributes);
        }
    
    });
    
});

