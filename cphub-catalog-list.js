#! /usr/bin/env node
/**
 *  Name:  cphub-catalog-list.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require('request');
var tools = require('./tools');
var standard_input = process.stdin;
var sprintf = require('sprintf-js').sprintf;
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const {program } = require('commander');

require('console.table');



program
    .option('-u, --url <URL>', 'CPFlex Registry base URL', 'http://hub.cpflex.tech/registry/v0.5')        
    .option('-c, --csv <csvfile>', 'Output catalog list to specified CSV file.')        
    .option('-l, --limit <limit>', 'Specifies the maximum number of records to return.', '-1')        
    .option('-p, --page <size>', 'Specifies the page size.  Default is not paged.', '-1')        

tools.addCommonOptions(program);
program.parse( process.argv);
tools.logHeader(program);


program.limit = Number(program.limit);
program.page =  Number(program.page);

function GetCatalogPage( limit, pageSize, pageIndex, pageState, handler)
{
    var input = {
        "etid":"cphub-catalog-list",
        "limit": limit,
        "pagingSize" : pageSize,
        "pagingIndex" : pageIndex,
        "pagingState" : pageState
    };

    var req = tools.getApiRequest( program, "/catalog/list", input);

    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);        
        handler( resp.hasMorePages, resp.pagingIndex, resp.pagingState, resp.items);
    });
}

function DisplayResponseHandler( hasMorePages, pagingIndex, pagingState, items)
{
    if( program.verbose === true)
    {
        console.info( JSON.stringify( items, null, 4));                
    }
    else
    {
        items.forEach(item => {
            console.info(sprintf( 
                "%-18s %s" 
               +"\r\n      Description:   %s" 
               +"\r\n      Updated:       %s"
               , item.sku, item.name, item.description, item.updated
           ));
        });
    }

    if( hasMorePages)
    {
        console.log(sprintf("\r\npagingIndex=%d", pagingIndex))
        console.log( "\r\nPlease press the enter key to list next page...\r\n");
        standard_input.on( 'data', function(data){
            GetCatalogPage( program.limit, program.page, pagingIndex, pagingState, DisplayResponseHandler);
        });
    }
    else
        process.exit(0);

}


function CsvResponseHandler( csvWriter, hasMorePages, pagingIndex, pagingState, items)
{
    csvWriter.writeRecords( items)
    .then( ()=>{
        if( hasMorePages)
        {
            GetCatalogPage( program.limit, program.page, pagingIndex, pagingState, function (hasMorePages, pagingIndex, pagingState, items){
                CsvResponseHandler( csvWriter, hasMorePages,  pagingIndex, pagingState, items )
            });
        }
        else
        {
            console.log('Completed writing CSV file');
            process.exit(0);
        }
    })

}

/****************************
 * Main Program
 ***************************/

if( program.csv === undefined)
{

    GetCatalogPage( program.limit, program.page, -1, [], DisplayResponseHandler);
}
else
{
    var  csvWriter;
    if( program.verbose === true)
        csvWriter = createCsvWriter({
            path: program.csv,
            header: [
                {id: 'sku', title:'sku'},
                {id: 'name', title:'name'},
                {id: 'description', title:'description'},
                {id: 'itemid', title:'itemid'},
                {id: 'profiletype', title:'profiletype'},
                {id: 'created', title:'created'},
                {id: 'updated', title:'updated'},
                {id: 'uridevicetype', title:'devicetype'},
                {id: 'idsubscription', title:'subscription'},
                {id: 'idprofile', title:'profile'}
            ]
        });
    else
        csvWriter = createCsvWriter({
            path: program.csv,
            header: [
                {id: 'sku', title:'sku'},
                {id: 'name', title:'name'},
                {id: 'description', title:'description'},
                {id: 'created', title:'created'},
                {id: 'updated', title:'updated'},
            ]
        });
        
    standard_input.setEncoding('utf-8');

    GetCatalogPage(program.limit, program.page, -1, [], function (hasMorePages, pagingIndex, pagingState, items){
        CsvResponseHandler( csvWriter, hasMorePages,  pagingIndex, pagingState, items )
    });
}
