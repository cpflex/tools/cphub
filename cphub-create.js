#! /usr/bin/env node
/**
 *  Name:  cphub-catalog-create.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require('request');
var tools = require('./tools');
const util = require('util');
const {program } = require('commander');

require('console.table');
var fs = require('fs');
program
    .description('Creates either a container or package node given the specified path and node type.  Can add optional label and other properties.',
    {
        'pathuri': 'Required pathuri URI having "[/]node1/node2/..." or "//hub.cpflex.tech/node1/node2/..." formats.',
        'nodetype': 'Node type specifier: either "package" or "container".',
        'nodespec': 'Optional node specification details in JSON format.'
    })
    .option('-s, --spec <specfile>', 'json file containing the node specification merged with [nodespec] if defined.')
    .option('-a, --access <visibility>', 'Specifies the access visibility, default is "same-as-parent", choice of "public", "account", "private" or "same-as-parent".')
    .option('-l  --label <label>',  'Optional label to provide user friendly term in user interfaces.  Default is not define and uses the pathuri.')
    .arguments('<pathuri> <nodetype> [nodespec]')
    .action( function( pathuri, nodetype, nodespec ){
        _nodespec = nodespec;
        _nodetype = nodetype;
        _pathuri = pathuri;
    });
tools.addCommonOptions(program);
program.parse( process.argv);
tools.logHeader(program);



//Define default visibility
let vis = 3; //Same as parent.
if( program.access === 'public') vis = 1;
else if( program.access === 'account') vis = 2;
else if( program.access === 'private') vis  = 4;
var inputspec = {
    visibility : vis,
    constraints : [],
    attributes : {}
};

if( !(program.label ===undefined))
    inputspec.label = program.label;

//Merfe input file if defined.
if( !(program.spec === undefined))
{
    var data = fs.readFileSync(program.spec, 'utf8');
    var json = JSON.parse( data.toString());    
    tools.mergeObjects(inputspec, json);
}

//Merge commandline if defined
if( !(_nodespec === undefined))
{
    tools.mergeObjects( inputspec, JSON.parse(_nodespec));
}

//Create the default structure.
var input = {
    etid: "cphub-create",
    nodeuri: _pathuri,
    nodetype: _nodetype,
    attributes : {}
};
tools.mergeObjects( input, inputspec);

//Dump information about the push.
if( program.verbose === true) {
    console.log(' node details ' );
    console.log( JSON.stringify( input, null, 2));
    console.log(' ');
}



var req = tools.getApiRequest( program, "/nodes/add", input);

var bytesSent = req.body.length;

request( req, (error, response, body) => {
    var resp = tools.parseResponseBody(error, response, body);
    console.log("node created");
});

