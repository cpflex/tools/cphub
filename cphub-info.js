#! /usr/bin/env node
/**
 *  Name:  cphub-info.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require('request');
const {program } = require('commander');
var tools = require('./tools');
const util = require('util');
require('console.table');


program
    .description("Gets details on nodes or tags.", {
        "pathuri": 'Required pathuri URI having "[/]node1/node2/..." or "//hub.cpflex.tech/node1/node2/..." formats.'
    })
    .option('-v, --verbose', 'Verbose output.')
    .option('-a, --absolute', 'Displays absolute path instead of relative.')
    .option('-J, --json', 'formats output as json. Useful when capturing in a file.', false )
    .arguments('<pathuri>')
    .action( function( pathuri){
        _pathuri = pathuri;
    });
tools.addCommonOptions(program);
program.parse( process.argv);


tools.logHeader(program);
console.log('Info: ' + _pathuri);
console.log('');

var _table = [];


function StoreTags( nodeuri, tags)
{
    let lenPath = _pathuri.length;

    tags.forEach( function(item){
        let taguri = nodeuri + ":" + item.tag;
        var pos = taguri.indexOf(_pathuri) + _pathuri.length + 1;
        let path = (program.absolute === true) ? taguri : taguri.substr(pos);
        _table.push( {
            'tag' : path,
            'created' : item.created
        });
    });        

}
function Report(node)
{
    _table.sort((a,b)=>(a.path > b.path) ? 1:-1);

    if( program.json === true) {
        node.tags = _table;
        console.log( JSON.stringify(node, null, 2));
    } else {
        //console.table(node);
        console.log("NODE DETAILS:");
        console.log(util.inspect(node, false, null, true));    
        console.log("\r\nACTIVE TAGS:");
        console.table( _table);
    }
}
/**
 * Function retrieves tags for the specified node.
 * @param {*} nodeuri 
 */
function GetTagList( nodeuri, handler) {

    var input = {
        "etid":"cphub-ls",
        "nodeuri": nodeuri
    };

    var req = tools.getApiRequest( program, "/tags/list", input);

    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        StoreTags(nodeuri, resp.tags);

        handler();
    });
}

/**
 * Function retrieves Node information.
 * @param {*} nodeuri 
 */
function GetNodeInfo( nodeuri, handler) {

    var input = {
        "etid":"cphub-ls",
        "nodeuri": nodeuri
    };

    var req = tools.getApiRequest( program, "/nodes/get", input);

    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        handler( resp.node);
    });
}

/**
 * Function retrieves Tag information.
 * @param {*} taguri 
 */
function GetTagInfo( taguri, handler) {

    var input = {
        "etid":"cphub-ls",
        "taguri": taguri
    };

    var req = tools.getApiRequest( program, "/tags/get", input);
    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        handler( resp.tag);
    });
}

/******************************************************
 * Begin processing information.
******************************************************/

var pos = _pathuri.indexOf(':');
let bIsTagRequest = pos >= 0;

if( bIsTagRequest)
{
    GetTagInfo( _pathuri, function(tag){
        if( program.json === true)
            console.log( JSON.stringify(tag, null, 2));
        else
            console.log(util.inspect(tag, false, null, true));        
    });
}
else
{
    GetNodeInfo(  _pathuri, function( node) {
        if( node.nodetype == 'package' && program.verbose === true) {

            GetTagList( _pathuri, function() {
                Report(node);
            });
        }
    });
}