#! /usr/bin/env node
/**
 *  Name:  cphub-copytags.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const { program } = require("commander");
var tools = require("./tools");
const util = require("util");
var fs = require("fs");
require("console.table");

program
  .description(
    "Copies a package's tags from the specified hub to the default hub(w/apikey).",
    {
      nodeuri:
        'Required nodeuri URI having "[/]node1/node2/..." or "//hub.cpflex.tech/node1/node2/... formats.',
      srcurl: "Source hub url",
    }
  )
  .option(
    "-x, --srckey <srckey>",
    "CPFlex account API Key for the source URL if not the same as target",
    ""
  )
  .arguments("<nodeuri> <srcurl>")
  .action(function (nodeuri, srcurl) {
    _nodeuri = nodeuri;
    _srcurl = srcurl;
  });

tools.addCommonOptions(program);

program.parse(process.argv);
tools.logHeader(program);

function promiseRequest(program, path, body, url = null, apikey = null) {
  var req = tools.getApiRequest(program, path, body);
  if (url != null) req.url = url + path;

  //Override the apikey if defined.
  if (apikey != null) {
    let headers = req.headers;
    delete headers.apikey;
    delete headers["x-consumer-custom-id"];
    var idx = apikey.indexOf("acctid|");
    var keytag;
    if (idx >= 0) {
      apikey = apikey.substr(idx + 7);
      keytag = "x-consumer-custom-id";
    } else keytag = "apikey";

    headers[keytag] = apikey;
  }

  //console.info("Request:\r\n" + JSON.stringify(req, null, 2));

  return new Promise((resolve, reject) => {
    request(req, (error, response, body) => {
      if (error != null) {
        console.log("HTTP Connection Error: " + error.message);
        process.exit();
      } else if (response.statusCode < 200 || response.statusCode > 299) {
        console.log("HTTP Error: " + response.statusCode);
        if (
          body !== null &&
          response.headers["content-type"].indexOf("application/json") > -1
        ) {
          var message = JSON.parse(body);
          console.log(util.inspect(message, false, null, true));
        } else console.log(body);

        process.exit();
      }

      if (body.length === 0) resolve(body);
      else resolve(JSON.parse(body));
    });
  });
}

/**
 * Function retrieves Node information.
 * @param {*} nodeuri
 */
function promiseGetNodeInfo(nodeuri, url = null, apikey = null) {
  var input = {
    etid: "cphub-ls",
    nodeuri: nodeuri,
  };
  return promiseRequest(program, "/nodes/get", input, url, apikey);
}

function promiseGetTagList(nodeuri, url = null, apikey = null) {
  var input = {
    etid: "cphub-ls",
    nodeuri: nodeuri,
  };
  return promiseRequest(program, "/tags/list", input, url, apikey);
}

/**
 * Function retrieves Tag information.
 * @param {*} taguri
 */
function promiseGetTagInfo(taguri, url = null, apikey = nully) {
  var input = {
    etid: "cphub-ls",
    taguri: taguri,
  };
  return promiseRequest(program, "/tags/get", input, url, apikey);
}

/**
 * Function retrieves Tag information.
 * @param {*} taguri
 */
function promiseGetImage(imguri, url = null, apikey = null) {
  var input = {
    etid: "cphub-ls" + imguri,
    imageuri: imguri,
  };
  return promiseRequest(program, "/images/pull", input, url, apikey);
}

/******************************************************
 * Begin processing information.
 ******************************************************/

console.log("");
console.log(
  "Copying: " +
    _nodeuri +
    " \r\n  src=" +
    _srcurl +
    "\r\n  tgt=" +
    tools.getUrl(program)
);
console.log("");

async function main() {
  let promises = [];

  let _srckey = program.srckey != "" ? program.srckey : null;

  //Get the package and tag list.
  promises.push(promiseGetNodeInfo(_nodeuri, _srcurl, _srckey));
  promises.push(promiseGetTagList(_nodeuri, _srcurl, _srckey));

  let results = await Promise.all(promises);

  let data = {
    pkg: results[0].node,
    tags: [],
  };

  //Get the tag info for each tag.
  promises.length = 0;
  console.log(util.inspect(results[1].tags, false, null, true));
  for (const tag of results[1].tags) {
    promises.push(
      promiseGetTagInfo(_nodeuri + ":" + tag.tag, _srcurl, _srckey)
    );
  }

  //Save the tag info.
  results = await Promise.all(promises);
  results.forEach((item) => {
    data.tags.push(item.tag);
  });

  // Now create / update the package.
  //console.log(util.inspect(data, false, null, true));
  //start pushing the tag.

  let tagNew = await promiseGetNodeInfo(_nodeuri);
  if (tagNew.resultcode != 1) {
    let pkgNew = {
      attributes: {},
      nodeuri: data.pkg.name,
      nodetype: data.pkg.nodetype,
      visibility: data.pkg.visibility,
    };

    console.info("Creating Package:\r\n" + JSON.stringify(pkgNew, null, 2));
    let res = await promiseRequest(program, "/nodes/add", pkgNew);
    if (res.resultcode != 1) {
      console.info(JSON.stringify(res, null, 2));
      console.error("cannot create package");
      process.exit();
    }
  }

  promises.length = 0;
  data.tags.forEach((tag) => {
    let imguris = tag.imageuris;
    let tagid = tag.tag;
    tag.etid = "cphub-copy";
    tag.taguri = _nodeuri + ":" + tag.tag;
    delete tag.created;
    delete tag.tag;
    delete tag.imageuris;

    //console.log('Pushing tag: \r\n' + util.inspect(tag, false, null, true));
    console.log("Pushing tag: " + tag.taguri);

    //Pull and push each image.
    imguris.forEach((uri) => {
      console.log("Pulling image:\r\n" + util.inspect(uri, false, null, true));
      let pgi = promiseGetImage(uri, _srcurl, _srckey).then((img) => {
        //if (img.image !== undefined)
        //    delete img.image.image;
        //console.log(util.inspect(img, false, null, true));
        delete img.image.imageid;
        let body = {
          etid: tag.etid,
          taguri: tag.taguri,
          images: [img.image],

          attributes: {},
          references: [],
          constraints: [],
        };

        //console.log(util.inspect(body, false, null, true));
        console.log("Pushing image:" + tagid + "/" + img.image.imagetype);
        return promiseRequest(program, "/images/push", body).then((result) => {
          if (result.resultcode != 1) {
            console.error(
              "Err pushing image: " +
                tagid +
                "/" +
                img.image.imagetype +
                "-" +
                JSON.stringify(result)
            );
            return result.resultcode;
          }
        });
      });

      promises.push(pgi);
    });
    //Just push tag information.
    if (tag.attributes == null) tag.attributes = {};
    if (!tag.hasOwnProperty("state")) tag.state = 0;

    promises.push(
      promiseRequest(program, "/tags/update", tag).then((result) => {
        if (result.resultcode != 1) {
          console.error("Err updating tag: " + tagid + JSON.stringify(result));
          console.log(JSON.stringify(tag, null, 2));
          return result.resultcode;
        }
      })
    );
  });

  await Promise.all(promises);
}

main();
