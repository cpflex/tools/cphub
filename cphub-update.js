#! /usr/bin/env node
/**
 *  Name:  cphub-update.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require('request');
var tools = require('./tools');
const util = require('util');
const {program } = require('commander');

require('console.table');
var fs = require('fs');
program
    .description("Updates a node or tag information given a json spec or file.", {
        "pathuri": 'Required pathuri URI having "[/]node1/node2/...[:tag]" or "//hub.cpflex.tech/node1/node2/...[:tag]" formats.',
        "tagspec": 'tag specifications to update.'
    })
    .option('-s, --spec <specfile>', 'json file containing the image specification merged with [tagspec] if defined.')
    .option('-l, --latest', 'Update latest tag with reference to this tag.')    
    .arguments('<pathuri> [tagspec]')
    .action( function(  pathuri, tagspec){
        _tagspec = tagspec;
        _pathuri = pathuri;
    });
tools.addCommonOptions(program);
program.parse( process.argv);
tools.logHeader(program);

//Load input spec if defined.
var inputspec = {
};

//Merge input file if defined.
if( !(program.spec ===undefined))
{
    var data = fs.readFileSync(program.spec, 'utf8');
    var json = JSON.parse( data.toString());
    tools.mergeObjects(inputspec, json);
}

//Merge commandline if defined
if( !(_tagspec === undefined))
{
    tools.mergeObjects( inputspec, JSON.parse(_tagspec));
}

//Create the default structure.
var input = {
    etid: "cphub-tag/update",
    taguri: _pathuri,
    references : [],
    constraints : [],
    attributes : {},
    state:0
};

tools.mergeObjects( input, inputspec);

//Dump information about the push.
if( program.verbose === true) {
    console.log(' tag details ' );
    console.log( JSON.stringify( input, null, 2));
    console.log(' ');
}

function UpdateTag(input, api, handler) {

    var req = tools.getApiRequest(program, api, input);
    request(req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);

        handler(resp.resultcode, req.body.length);
    });
}

//Call the update API to update specified tag.
UpdateTag(input, "/tags/update", function (rc, ctSent) {

    if (program.verbose === true)
        console.log("Updated: " + _pathuri);

    //Update the latest reference if specified.
    if (program.latest === true && rc == 1) {
        var pos = _pathuri.indexOf(':');
        var latesturi = _pathuri.substr(0, pos) + ":latest";
        var latest = {
            etid: "cphub-tag-update",
            taguri: latesturi,
            state: 0,
            references: [
                _pathuri
            ],
            created: new Date(),
            description: input.description,
            constraints: input.constraints,
            attributes: input.attributes
        };

        //MBM 2021/07/22 To Support Tag Types, we set the tag type
        //attribute to reference.
        latest.attributes.tagtype = "reference";

        UpdateTag(latest, "/tags/update", function (rc, ctSent) {
            if (program.verbose === true)
                console.log("Updated: " + latesturi);
        });
    }
});