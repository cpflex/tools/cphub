# CP-Flex&trade; HubCommand Line Tools

Node JS based tools provide a easy-to-use command line interface for interacting
with the CP-Flex Hub.  The is the CP-Flex application registry where providers can obtain 
specifications, and application packages for CP-Flex powered devices.  
Access to the hub requires a valid account API key,  please contact Codepoint 
support if you need an account and key set.

## Installation and Setup

### Install Node.js  

The tools require Node.js (javascript) tools to install your system.   If not 
already installed, please find the package for your 
system an install it. Go to https://nodejs.org/en/download/ and select your OS.
Follow the instructions there.

### Clone and Install cphub
The tools here are essentially scripts that can be packaged and deployed 
globally on your system. This provides a command line interface for working with 
CP-Flex HUB APIs.   

1. Install Git.  The best way to obtain the tools is to use GIT this can be 
   found at https://git-scm.com/downloads 

1. Clone this project to your local computer. Open a command window or 
   terminal, change to a working directory, where you want to clone the source files 
   (e.g. .../projects/cpflex/) and execute the following comand.  
    ```
    clone https://gitlab.com/cpflex/tools/cphub.git
    ```

1. Once cloned, change directory into the cpacc project and install the the package locally
    ```
    npm install
    ```
    For developers, the source code is freely available to see how to use the APIs.   

1. To access the tools from anywhere on your machine, package them as a single executable. 
   Install pkg to create a single binary and then use the tool to create executables for 
   Windows, MacOS, or linux.
    ```
    npm install -g pkg
    pkg .
    ```
    This will create cphub-win.exe, cphub-linux, and cphub-macos.

1. Select the package for you OS and copy it to a folder that is on your path in order to 
   execute globally.  For example, copy the *cphub-win.exe* to a tools folder (e.g. `C:/bin`) 
   and rename the file, *cphub.exe*.

1. Once installed, change your working directory to a project folder and execute the following command:
    ``` 
    > cphub 
    Usage: cphub [options] [command]

    Options:
      -V, --version                                       output the version number
      -h, --help                                          display help for command

    Commands:
      create <path> [nodespec]                            create a container  or package node.
      info <path>                                         get information about a node or tag
      list <pathuri>                                      lists the contents of a container or package.
      push <imagetype> <imagefile> <path:tag> [pushspec]  push tagged image data to a package.
      remove <path[:tag]>                                 removes node or tag from registry.
      update <path:tag> [tagspec]                         update tag specifcation data.
      pull <pathuri>                                      pull tagged image data from a package
      catalog                                             lists catalog items
      catalog <sku>                                       retrieves detailed catalog item information.
      help [command]                                      display help for command
    ```
    If successful, the main help menu should be shown as above.


1.  Set up you environment variables with your account apikey and access key:  
    * On Windows:<br/>
        ```
        set CPFLEX_APIKEY="<YOUR API KEY>"
        ```

    * On Linux or similar systems
        ```
        declare -x CPFLEX_APIKEY='<YOUR API KEY>"
        ```
    This environment variable can be made permanent by storing in your system configuration. 
    You can also add these on the command line (the '-K' option) 
    if you are changing API KEYS often or don't want them to persist.   

## Using the Hub
*cphub* provides a thin wrapper over the API's facilitating interaction with the hub.  Other Codepoint
tools implement direct support with the hub to provide customers easy access to applications and 
firmware.
  
All data used by the tools are provided either on the command line or by separate JSON 
files. 

The hub is a structured registry of nodes: either a general container or package node supporting tagged images.
These nodes ares secured using the CP-Flex account system.   A node's visibility can be set as 
public, account (soon to be changed to protected), and private.

* *Public* nodes are visible to everyone
* *Account/Protected* nodes are visible only to an account and its sub-accounts.
* *Private* nodes are visible only the the account owner.

For now, users are free to create their own root nodes as long as their account is valid.  The 
recommended practice is to define your root node public using the name of your organization.  Then
place all other nodes within your organization. 

For example:

```
<myorg node>
    /<applications>
    /<test>
```
### Creating Containers and Packages

The following demonstrates creating a set of containers and an package node for storing published 
applications.

```
cphub create myorg container -a public -l "My Organization"
cphub create myorg/apps container -a public 
create myorg/apps/app1 package -a public -l "My Org Application 1" "{\"description\":\"This an example application that shows how to use the hub\"}"
```

### Listing Nodes and Packages


To view a set of  containers and packages the cphub list command can be used. For example, the following commands
show how to get about multiple nodes.

List all nodes in my org with full detail and absolute paths.
```
> cphub list myorg -r -a -v

Listing: myorg

path                               type       visibility  created
---------------------------------  ---------  ----------  ------------------------
//hub.cpflex.tech/myorg/apps       container  public      2020-11-07T23:49:23.127Z
//hub.cpflex.tech/myorg/apps/app1  package    public      2020-11-08T00:11:52.212Z
```

The list command can also be configured to filter on node type.

### Get Information on a Package or Container

```
> cphub info myorg/apps/app1

Info: myorg/apps/app1

key          value
-----------  ---------------------------------------------------------
attributes   [object Object]
created      2020-11-08T00:11:52.212Z
description  This an example application that shows how to use the hub
label        My Org Application 1
name         //hub.cpflex.tech/myorg/apps/app1
nodetype     package
visibility   1
```

### Pushing / Pulling Tagged Images to a Package
To push an image into a package use the cphub push command. Each package can hold 
any number of tags.  Older tags can also be deprecated by setting the state to 1.
So they are not visible when customers are searching the repository.

Image size is currently limited to < 10 MB.  Normally images should be be less than 1MB as
applications are not typically large.

#### Pushing an Image

1. Create a readme.md file with a text string:  "this is a readme file".
2. Push the readme file into the app1 package with tag 'readme'
    ```
    cphub push application/text readme.md myorg/apps/app1:readme
    ```
3. Retrieve information about the package and tagged image.
    ```
    > C:\dev>cphub info myorg/apps/app1 -v

    URL:    http://global.cpflex.tech/hub/v1.0
    Info: myorg/apps/app1

    key          value
    -----------  ---------------------------------------------------------
    attributes   [object Object]
    created      2020-11-08T00:11:52.212Z
    description  This an example application that shows how to use the hub
    label        My Org Application 1
    name         //hub.cpflex.tech/myorg/apps/app1
    nodetype     package
    visibility   1

    tag     created
    ------  ------------------------
    readme  2020-11-08T00:24:08.039Z


    > C:\dev>cphub info myorg/apps/app1:readme
    Info: myorg/apps/app1:readme

    key          value
    -----------  ---------------------------------------
    attributes   [object Object]
    constraints
    created      2020-11-08T00:24:08.039Z
    imageuris    RsKABq2AQWaU4bIe4NXVZA/application/text
    references
    state        0
    tag          readme
    ```

#### Pulling an Image
Pulling an image is fairly straight forward.  

```
> Pulling: myorg/apps/app1:readme

Saving: hub.cpflex.tech-myorg-apps-app1--readme.bin
```

If the image was saved with original file name, the '-o' option can be used to set the
file to the original name.   cphub pushes the original file name by default.

```
C:\dev>cphub pull myorg/apps/app1:readme -o

Pulling: myorg/apps/app1:readme

Saving: readme.md
```


### Performing Constraint Search

Information to come.

### Setting a Package References, Constraints, and Attributes
Packages can specify references, constraints, and attributes that define how images
relate to other items in the hub.  These properties enable the tools to determine dependencies and
updates that are required when applying images.   

More details to come.


---
*Copyright &copy; 2020 Codepoint Technologies, Inc.*<br/>
*All Rights Reserved*

