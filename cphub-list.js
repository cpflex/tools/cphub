#! /usr/bin/env node
/**
 *  Name:  cphub-list.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require('request');
const { program } = require('commander');
var tools = require('./tools');
require('console.table');


program
    .description("lists nodes or tags", {
        "pathuri": 'Require pathuri URI having "[/]node1/node2/..." or "//hub.cpflex.tech/node1/node2/..." formats.'
    })
    .option('-r, --recurse  <depth>', 'Recurse sub-containers. -1 will recurse all depths, 0 is current container or package.', 0)
    .option('-d, --deprecate', 'Show deprecated tags.', false)
    .option('-f, --filter <mode>', 'Filter mode options: none (default), nodes, packages, containers, tags', 'none')
    .option('-a, --absolute', 'Displays absolute path instead of relative.')
    .arguments('<pathuri>')
    .action(function (pathuri) {
        _pathuri = pathuri;
    });
tools.addCommonOptions(program);
program.parse(process.argv);
tools.logHeader(program);

//Set recurse to ridiculously large depth if -1.
if( program.recurse < 0 )
    program.recurse = 0x1000000;

console.log('');
console.log('Listing: ' + _pathuri);
console.log('');

var _table = [];

function StoreNodes(nodes) {
    if (program.filter == 'tags')
        return;

    nodes.forEach(function (item) {
        if (program.filter == 'packages' && item.nodetype != 'package')
            return;
        else if (program.filter == 'containers' && item.nodetype != 'container')
            return;

        var pos = item.name.indexOf(_pathuri) + _pathuri.length;
        let path = (program.absolute === true) ? item.name : item.name.substr(pos);
        let vis = (item.visibility == 1) ? "public" : (item.visibility == 2) ? "account" : "private";
        _table.push({
            'path': path,
            'type': item.nodetype,
            'visibility': vis,
            'state': 'active',
            'created': item.created,
            'nodeuri': item.name,
        });

    });

}

function StoreTags(nodeuri, tags) {
    let lenPath = _pathuri.length;

    if (program.filter == 'none' || program.filter == 'tags') {

        tags.forEach(function (item) {
            let taguri = nodeuri + ":" + item.tag;
            var pos = taguri.indexOf(_pathuri) + _pathuri.length;
            let path = (program.absolute === true) ? taguri : taguri.substr(pos);

            _table.push({
                'path': path,
                'type': 'tag',
                'visibility': '-n/a',
                'state' : item.state == 1 ? 'deprecated' : 'active',
                'created': item.created,
                'nodeuri': nodeuri,
            });
        });
    }
}
function Report() {
    _table.sort((a, b) => {
        if( a.nodeuri == b.nodeuri )
        {
            if( a.type == 'tag' && b.type == 'tag')
                return  (a.tag == 'latest' || a.created > b.created) ? -1 : 1;
            else if( a.type != 'tag')
                return -1;
            else
                return 1;
        }
        else
            return (a.nodeuri > b.nodeuri) ? 1 : -1;
    });

    var report = [];
    _table.forEach( function (item ) {
        if (program.verbose === true) {
            report.push({
                'path': item.path,
                'type': item.type,
                'state' : item.state,
                'visibility': item.visibility,
                'created': item.created
            });

        }
        else {
            report.push({
                'path': item.path,
                'type': item.type,
                'state' : item.state
            });
        }
    });
    console.table(report);
}

var ctRequests = 0;
var ctReceived = 0;



/**
 * Function retrieves subnodes for the specified path.
 * @param {*} nodeuri 
 */
function GetNodeList(nodeuri, recurse) {

    var input = {
        "etid": "cphub-ls",
        "nodeuri": nodeuri
    };

    var req = tools.getApiRequest( program, "/nodes/list", input);
    ctRequests++;

    request(req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);

        let nodes = resp.nodes;
        StoreNodes(nodes);


        nodes.forEach(function (item) {
            if (item.nodetype == 'container' && recurse > 0) {
                GetNodeList(item.name, recurse-1);
            }
            else if (item.nodetype=='package' &&  (program.filter == 'none' || program.filter == 'tags'))
                GetTagList(item.name);
        });

        ctReceived++;

        if (ctReceived >= ctRequests)
            Report();

    });
}

/**
 * Function retrieves tags for the specified node.
 * @param {*} nodeuri 
 */
function GetTagList(nodeuri) {

    var input = {
        "etid": "cphub-ls",
        "nodeuri": nodeuri,
        "deprecated": program.deprecate
    };

    var req = tools.getApiRequest( program, "/tags/list", input);
    ctRequests++;

    request(req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        ctReceived++;

        StoreTags(nodeuri, resp.tags);
        if (ctReceived >= ctRequests)
            Report();
    });
}

/**
 * Function retrieves Node information.
 * @param {*} nodeuri 
 */
function GetNodeInfo(nodeuri, handler) {

    var input = {
        "etid": "cphub-ls",
        "nodeuri": nodeuri
    };

    var req = tools.getApiRequest( program, "/nodes/get", input);
    ctRequests++;

    request(req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        ctReceived++;
        handler(resp.node);
    });
}

/******************************************************
 * Begin processing information.
******************************************************/

GetNodeInfo(_pathuri, function (node) {
    if (node.nodetype == 'container') {
        GetNodeList(node.name, program.recurse);
    }
    else if (program.filter == 'none' || program.filter == 'tags')
        GetTagList(node.name);
})

