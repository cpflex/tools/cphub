#! /usr/bin/env node
/**
 *  Name:  cphub-catalog-info.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require('request');
var sprintf = require('sprintf-js').sprintf;
var tools = require('./tools');
const {program } = require('commander');

require('console.table');



program
    .arguments('<sku>', 'Required catalog SKU for the item.')
    .action( function( sku){
        _sku = sku;
    });
tools.addCommonOptions(program);
program.parse( process.argv);


tools.logHeader(program);
console.log('SKU: ' + _sku);
console.log('');

var _table = [];




/**
 * Function retrieves the catalog item information.
 * @param {*} nodeuri 
 */
function GetCatalogItem( sku, handler) {

    var input = {
        "etid":"cphub-catalog-info",
        "sku": sku
    };

    var req = tools.getApiRequest( program, "/catalog/get", input);

    request( req, (error, response, body) => {
        var resp = tools.parseResponseBody(error, response, body);
        handler(resp.item);
    });
}

GetCatalogItem(_sku, function(item){

    if( program.verbose === true)
        console.info( JSON.stringify( item, null, 4));
    else
    {
        console.info(sprintf( 
             "%s" 
            +"\r\n----------------------------------------------------------------------"
            +"\r\nDescription:   %s" 
            +"\r\nUpdated:       %s"
            , item.name, item.description, item.updated
        ));

        if( !(typeof item.skus === 'undefined'))
        {
            for( i = 0; i < item.skus.length; i++)
            {   
                var sfmt =  "               %s";
                if( i == 0) sfmt = "SKUs:          %s";

                console.log(sprintf(sfmt, item.skus[i]));
            }
        }
 
    }

});