#! /usr/bin/env node
/**
 *  Name:  tools.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020, 2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const DEFAULT_PROTOCOL = "http";
const DEFAULT_DOMAIN = "global.cpflex.tech";
const DEFAULT_PATH = "hub/v1.0";

/*
 * * Recursively merge properties of two objects
 * */
function _mergeObjects(obj1, obj2) {
  for (let p in obj2) {
    try {
      // Property in destination object set; update its value.
      if (obj2[p].constructor == Object) {
        obj1[p] = _mergeObjects(obj1[p], obj2[p]);
      } else {
        obj1[p] = obj2[p];
      }
    } catch (e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];
    }
  }
  return obj1;
}

function _parseResponseBody(error, response, body) {
  if (error != null) {
    console.log("HTTP Connection Error: " + error.message);
    process.exit();
  } else if (response.statusCode < 200 || response.statusCode > 299) {
    console.log("HTTP Error: " + response.statusCode);
    process.exit();
  }

  var resp = JSON.parse(body);

  if (resp.resultcode != 1) {
    console.log(
      "CP-Flex Hub error (code=" + resp.resultcode + "): " + resp.errinfo
    );
    process.exit();
  }
  return resp;
}

function _addCommonOptions(program) {
  program.option(
    "-u, --url <url>",
    'Overrides default or "CPHUB_URL" or "CPFLEX_DOMAIN" (global.cpflex.tech) environment variable.',
    _getDefaultUrl()
  );
  program.option(
    "-k, --apikey <apikey>",
    'CPFlex account API Key must be defined or specified in environment variable "CPFLEX_APIKEY" '
  );
  program.option("-v, --verbose", "Verbose output.");
}

function _stripQuotes(str) {
  return str.replace(/^"|"$/g, "");
}

function _getDefaultUrl() {
  let domain = DEFAULT_DOMAIN;
  if (process.env.CPFLEX_DOMAIN) {
    domain = _stripQuotes(process.env.CPFLEX_DOMAIN);
  }
  return `${DEFAULT_PROTOCOL}://${domain}/${DEFAULT_PATH}`;
}

function _getUrl(program) {
  if (
    program.url == _getDefaultUrl() &&
    !(process.env.CPHUB_URL === undefined)
  ) {
    return _stripQuotes(process.env.CPHUB_URL);
  } else return program.url;
}

function _getApiKey(program) {
  if (program.apikey === undefined) {
    if (
      process.env.CPFLEX_APIKEY === undefined ||
      process.env.CPFLEX_APIKEY.length == 0
    )
      return null;
    else return _stripQuotes(process.env.CPFLEX_APIKEY);
  } else return program.apikey;
}

function _logHeader(program) {
  if (program.verbose === true) {
    console.log("");
    console.log("URL:    " + _getUrl(program));
    console.log("APIKEY: " + _getApiKey(program));
  }
}

function _getCommonHeader(program) {
  let keyval = _getApiKey(program);
  if (keyval == null)
    throw '"--apikey" option  and CPFLEX_APIKEY environment variable are note defined.  Cannot call service without access credentials.';

  var idx = keyval.indexOf("acctid|");
  if (idx >= 0) {
    keyval = keyval.substr(idx + 7);
    keytag = "x-consumer-custom-id";
  } else keytag = "apikey";

  var headers;

  if (keyval == "ignore") {
    headers = {
      "Content-Type": "application/json",
      Accept: "application/json",
    };
  } else {
    headers = {
      "Content-Type": "application/json",
      Accept: "application/json",
      [keytag]: keyval,
    };
  }
  return headers;
}

function _getApiRequest(program, path, body) {
  var req = {
    url: _getUrl(program) + path,
    method: "POST",
    headers: _getCommonHeader(program),
    body: JSON.stringify(body),
  };
  return req;
}

module.exports = {
  getApiKey: _getApiKey,
  getUrl: _getUrl,
  logHeader: _logHeader,
  parseResponseBody: _parseResponseBody,
  mergeObjects: _mergeObjects,
  addCommonOptions: _addCommonOptions,
  getCommontHeader: _getCommonHeader,
  getApiRequest: _getApiRequest,
};
